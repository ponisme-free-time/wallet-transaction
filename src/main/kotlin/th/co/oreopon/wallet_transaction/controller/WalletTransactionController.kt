package th.co.oreopon.wallet_transaction.controller

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*
import th.co.oreopon.wallet_transaction.model.WalletTransaction
import th.co.oreopon.wallet_transaction.model.reponse.BaseRestApi
import th.co.oreopon.wallet_transaction.service.WalletTransactionService

@RestController
@RequestMapping("/v1/wallet_transactions")
class WalletTransactionController(@Autowired  var walletTransactionService: WalletTransactionService) {
    @PostMapping("/savedata")
    fun savedata(@RequestBody walletTransaction: WalletTransaction):ResponseEntity<BaseRestApi<String>>{
        return walletTransactionService.savedata(walletTransaction)
    }
    @GetMapping("/getdata")
    fun getData(@RequestParam(defaultValue = "10") size:Int, @RequestParam(defaultValue = "0") page:Int):ResponseEntity<BaseRestApi<Page<WalletTransaction>>>{
        return walletTransactionService.getData(size,page);
    }
}