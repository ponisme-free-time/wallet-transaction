package th.co.oreopon.wallet_transaction

import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.runApplication

@SpringBootApplication
class WalletTransactionApplication

fun main(args: Array<String>) {
	runApplication<WalletTransactionApplication>(*args)
}
