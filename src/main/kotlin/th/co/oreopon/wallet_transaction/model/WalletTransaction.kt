package th.co.oreopon.wallet_transaction.model

import org.springframework.data.mongodb.core.mapping.Document
import org.springframework.data.mongodb.core.mapping.Field
import java.math.BigDecimal
import java.time.LocalDate
@Document(collection ="wallet_transaction")
data class WalletTransaction(
    @Field("wallet_from")
    var walletFrom:String,
    @Field("wallet_to")
    var walletTo:String,
    @Field("wallet_amount")
    var walletAmount:BigDecimal,
    @Field("created_date")
    var createdDate:LocalDate=LocalDate.now())  {
}