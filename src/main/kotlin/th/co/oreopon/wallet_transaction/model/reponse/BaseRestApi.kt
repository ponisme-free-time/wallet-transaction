package th.co.oreopon.wallet_transaction.model.reponse

data class BaseRestApi<T>(var resultCode:String,var resultMessage:String,var data:T) {
}