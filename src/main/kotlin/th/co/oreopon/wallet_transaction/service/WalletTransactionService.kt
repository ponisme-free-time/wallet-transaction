package th.co.oreopon.wallet_transaction.service

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Service
import th.co.oreopon.wallet_transaction.model.WalletTransaction
import th.co.oreopon.wallet_transaction.model.reponse.BaseRestApi
import th.co.oreopon.wallet_transaction.repository.WalletTransactionRepository


@Service
class WalletTransactionService(@Autowired var walletTransactionRepository: WalletTransactionRepository) {

        fun savedata(walletTransaction: WalletTransaction):ResponseEntity<BaseRestApi<String>>{
            walletTransactionRepository.save(walletTransaction);
            val response=BaseRestApi<String>("0000","success","")
            return ResponseEntity<BaseRestApi<String>>(response,HttpStatus.OK);
        }

    fun getData(size: Int, page: Int): ResponseEntity<BaseRestApi<Page<WalletTransaction>>> {
        var pageable:Pageable=PageRequest.of(page,size);
        var responedata=walletTransactionRepository.findAll(pageable)
        var responseResult=BaseRestApi<Page<WalletTransaction>>("00","success",responedata)
        return ResponseEntity(responseResult,HttpStatus.OK)
    }


}