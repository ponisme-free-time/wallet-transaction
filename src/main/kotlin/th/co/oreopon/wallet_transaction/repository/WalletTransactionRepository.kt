package th.co.oreopon.wallet_transaction.repository

import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.mongodb.repository.MongoRepository
import th.co.oreopon.wallet_transaction.model.WalletTransaction


interface WalletTransactionRepository:MongoRepository<WalletTransaction,String> {

    override fun findAll(page:Pageable):Page<WalletTransaction>
}