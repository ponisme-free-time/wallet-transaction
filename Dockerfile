FROM gradle:7.6.1-jdk17 as builder
WORKDIR /app
COPY . /app
RUN gradle clean build

FROM eclipse-temurin:17-jre-alpine
EXPOSE 8080
COPY --from=builder /app/build/libs/wallet_transaction-0.0.1-SNAPSHOT.jar app.jar
#ADD /build/libs/wallet_transaction-0.0.1-SNAPSHOT.jar wallet_transaction.jar
ENTRYPOINT exec java -jar app.jar
 #["java", "-jar", "wallet_transaction.jar"]